interface Facts {
    id: number;
    fact: string;
    date: number;
    vote: number;
    points: number;
}
