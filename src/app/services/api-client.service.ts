import {Injectable} from '@angular/core';
import {HTTP} from '@ionic-native/http/ngx';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ApiClientService {

    private url: string;

    constructor(public httpC: HttpClient, public http: HTTP) {
    }

    public getRandomFacts(count: number, page: number) {
        return this.httpC.get(`https://www.chucknorrisfacts.fr/api/get?data=tri:alea;nb:${count};page:${page}`);
    }

    // @ts-ignore
    public getPopularFacts(count: number, page: number): Promise<any> {
        return this.http.get(`https://www.chucknorrisfacts.fr/api/get?data=tri:alea;nb:${count};page:${page}`, {}, {});
    }

}
